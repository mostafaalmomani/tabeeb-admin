import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './core/auth.guard';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { MainViewComponent } from './view/main-view/main-view.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';

const routes: Routes = [{
  path: 'auth',
  loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
},
{
  path: 'shared',
  loadChildren: () => import('./shared/shared.module').then(m => m.SharedModule)
},
{
  path: '',
  redirectTo: 'MainView/pages/dashboard',
  pathMatch: 'full'
},
{
  path: 'MainView',
  component: MainViewComponent,
  canActivate: [AuthGuard],
  resolve: {},
  children: [
    {
      path: 'pages',
      loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)
    }
  ]
},
{path: '**', component: PageNotFoundComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule],
  providers: [AuthGuard]

})
export class AppRoutingModule { }
