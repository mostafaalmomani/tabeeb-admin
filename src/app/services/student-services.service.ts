import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Student, User } from '../core/models';
import { Course } from '../core/models/course';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class StudentServicesService extends BaseService{

  constructor(private http: HttpClient) {
    super();
  }

  addStudent(student: User): any {
    return this.http.post<{message: any}>(`${this.apiUrl}/api/v1/register/teacher`, {student});
  }

  addStudents(students: any[]): any {
    return this.http.post<{message: any}>(`${this.apiUrl}/api/v1/register/student`, {students});
  }

  register(user: User): any {
    return this.http.post<{message: any }>(`${this.apiUrl}/api/v1/register`, user);
  }
  editStudent(user: User): any {
    return this.http.post<{message: any}>(`${this.apiUrl}/api/v1/updatesteacher`, {user});
  }

  getTeachers(): any {
    return this.http.get<{teachers: Student}>(`${this.apiUrl}/api/v1/getTeachers`);
  }

  deleteTeacher(id: string): any {
    return this.http.delete<{message: any}>(`${this.apiUrl}/api/v1/deleteteacher/${id}`);
  }

  getCourses(): any {
    return this.http.get<{courses: Course}>(`${this.apiUrl}/api/v1/getCourses`);
  }

  getStudent(): any {
    return this.http.get<{students: Student}>(`${this.apiUrl}/api/v1/getAllForAdmin`);
  }

  deleteStudent(id: string): any {
    return this.http.delete<{message: any}>(`${this.apiUrl}/api/v1/deletestudents/${id}`);
  }

  editStudents(student: Student): any {
    return this.http.post<{message: any}>(`${this.apiUrl}/api/v1/updatestudents`, {student});
  }
}
