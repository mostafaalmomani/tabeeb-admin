import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { User } from '../core/models';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationServiceService extends BaseService{

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  private isAuthenticated = false;
  private token: string;
  private tokenTimer: any;
  private userId: string;
  public fullName: string;
  private authStatusListener = new Subject<boolean>();
  constructor(private http: HttpClient, private _snackBar: MatSnackBar) {
    super();
    this.currentUserSubject = new BehaviorSubject<any>(sessionStorage.getItem('currentAdmin'));
    this.currentUser = this.currentUserSubject.asObservable();
  }


  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(email: string, password: string): any {

    return this.http.post<{ user: User }>(`${this.apiUrl}/api/v1/auth/admin`,
      { email, password })
      .pipe(map(user => {
        if (user && user['token']) {
          const token = user['token'];
          this.token = token;
          if (token) {
            const expiresInDuration = user['expiresIn'];
            sessionStorage.setItem('duration', expiresInDuration);
            this.isAuthenticated = true;
            // this.userId = user['id'];
            this.fullName = user['fullName'];
            this.authStatusListener.next(true);
            const now = new Date();
            const expirationDate = new Date(now.getTime() + expiresInDuration * 1000);
            this.saveAuthData(token, expirationDate, this.userId, this.fullName);
            this.currentUserSubject.next(user.user);

          }

        }

        return user;
      }));
  }
  getToken(): any {
    const token = sessionStorage.getItem('token');
    return token;
  }

  getIsAuth(): any {
    return this.isAuthenticated;
  }

  getUserId(): any {
    return sessionStorage.getItem('currentAdmin');
  }
  getAuthStatusListener(): any {
    return this.authStatusListener.asObservable();
  }

  register(data: FormData): any {
    return this.http.post<{ Teacher: User }>(`${environment.apiUrl}register`, data);
  }

  private saveAuthData(token: string, expirationDate: Date, userId: string, fullName: string): void {
    sessionStorage.setItem('token', token);
    sessionStorage.setItem('expiration', expirationDate.toISOString());
    sessionStorage.setItem('currentAdmin', "uwefwe54ser5465eId646165496464ad65r4g65dfg");
    sessionStorage.setItem('fullName', fullName);
  }

  public getAuthData() {
    const token = sessionStorage.getItem('token');
    const expirationDate = sessionStorage.getItem('expiration');
    const userId = sessionStorage.getItem('currentAdmin');
    if (!token || !expirationDate) {
      return;
    }
    return {
      token: token,
      expirationDate: new Date(expirationDate),
      userId: userId
    }
  }

  public setAuthTimer(duration: number) {
    console.log(duration)
    this.tokenTimer = setTimeout(() => {
      this.logout();
    }, duration * 6000);
  }


  logout(): void {
    this.http.get<{ isLoggedOut: boolean }>(`${this.apiUrl}/api/v1/logout`).subscribe(res => {
      sessionStorage.removeItem('currentAdmin');
      sessionStorage.removeItem('token');
      sessionStorage.removeItem('expiration');
      this.currentUserSubject.next(null);
      location.reload();
    });
    this.openSnackBar('انتهت الجلسة', 'err-snackbar');
  }

  deleteAll(): any {
    return this.http.post<{message: any}>(`${environment.apiUrl}deleteAllData`, {});
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 800,
      panelClass: [syle]
    });
  }
}
