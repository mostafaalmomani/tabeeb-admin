import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Exam, Questions, Rules, Student } from '../core/models';
import { Course } from '../core/models/course';
import { Mark } from '../core/models/mark';
import { MarkCourse } from '../core/models/markCourse';
import { AuthenticationServiceService } from './authentication-service.service';
import { BaseService } from './base.service';

@Injectable({
  providedIn: 'root'
})
export class ExamsService extends BaseService{

  constructor(private http: HttpClient, private authservices: AuthenticationServiceService) {
    super();
  }

  createExam(exam: Exam, rules: Rules, students: Student): Observable<any> {
    return this.http.post<{ok: any, exam: Exam, rules: Rules}>(`${this.apiUrl}/api/v1/createExam`, {exam, rules, students});
  }

  getExams(): Observable<any> {
    return this.http.post<{exams: Exam, rules: Rules}>(`${this.apiUrl}/api/v1/getExam`, {});
  }

  getStudents(id: string): any {
    return this.http.get<{students: Student[]}>(`${this.apiUrl}/api/v1/getStudents/${id}`);
  }

  getCardsInfo(): any {
    return this.http.get(`${this.apiUrl}/api/v1/cardInformation`);
  }

  getStudentsMark(id: string): any {
    return this.http.get<{marks: Mark[]}>(`${this.apiUrl}/api/v1/getStudentsMark/${id}`);
  }

  saveExamMark(mark: Mark): any {
    return this.http.post<{message: string, mark: Mark}>(`${this.apiUrl}/api/v1/saveExamMark`, {mark});
  }

  saveCourseMark(mark: MarkCourse): any {
    return this.http.post<{message: string}>(`${this.apiUrl}/api/v1/saveCourseMark`, {mark});
  }

  saveCourse(course: any): any {
    return this.http.post<{message: string}>(`${this.apiUrl}/api/v1/createCourse`, {course});
  }

  getCourses(): any {
    return this.http.get<{courses: Course}>(`${this.apiUrl}/api/v1/getCourses`);
  }

  getCoursesStudents(courseNumber: string): any {
    return this.http.get<{students: any, courseName: string, course: Course}>(`${this.apiUrl}/api/v1/getCoursesStudentsForConfirm/${courseNumber}`);
  }

  getStudentMarks(studentId: string, courseNumber: string): any {
    return this.http.post<{marks: Mark}>(`${this.apiUrl}/api/v1/getStudentMarks`, {studentId, courseNumber});
  }

  getStudentCourseMarks(studentId: string, courseNumber: string): any {
    return this.http.get<{mark: MarkCourse}>(`${this.apiUrl}/api/v1/getStudentCourseMark/${studentId}/${courseNumber}`);
  }

  saveOtherMark(mark: Mark): any {
    return this.http.post<{message: string, mark: Mark}>(`${this.apiUrl}/api/v1/saveOtherMark`, {mark});
  }

  saveQuestion(data: FormData): any {
    return this.http.post<{ question: Questions }>(`${this.apiUrl}/api/v1/saveQuestion`, data);
  }

  getRequests(): any {
    return this.http.get<{requests: any[]}>(`${this.apiUrl}/api/v1/requests`);
  }

  consfirm(course: any): any {
    return this.http.post<{ message: boolean }>(`${this.apiUrl}/api/v1/confirm`, {course: course});
  }

  getStudentAssessments(course: string): any {
    return this.http.post<{assessments: any}>(`${this.apiUrl}/api/v1/getAssessments`, {course:course});
  }

  deleteCourse(id: string): any {
    return this.http.delete<{message: any}>(`${this.apiUrl}/api/v1/deleteCourse/${id}`);
  }
}
