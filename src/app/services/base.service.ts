import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  apiUrl = 'https://api.exams-tabeeb.org'
  // apiUrl = 'http://localhost:3000'
  constructor() { }
}
