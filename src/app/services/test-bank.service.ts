import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Exam, Questions, Rules, Student } from '../core/models';
import { AuthenticationServiceService } from './authentication-service.service';
import { Observable } from 'rxjs';
import { BaseService } from './base.service';
@Injectable({
  providedIn: 'root'
})
export class TestBankService extends BaseService{

  constructor(private http: HttpClient, private authservices: AuthenticationServiceService) {
    super();
  }

  createTest(exam: Exam): Observable<any> {
    return this.http.post<{message: any, exam: Exam}>(`${this.apiUrl}/api/v1/createTest`, {exam});
  }

  saveQuestion(data: FormData): any {
    return this.http.post<{ question: Questions }>(`${this.apiUrl}/api/v1/saveTestBankQuestion`, data);
  }
}
