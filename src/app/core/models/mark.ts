export class Mark {
  _id?: string;
  markDesc: string;
  examId: string;
  studentsId?: string;
  markPoint?: number;
  courseNumber?: string;
}
