export class User {
  _id?: string;
  email: string;
  password: string;
  fullName?: string;
  token?: string;
  phoneNumber?: string;
  pictuer?: string;
  course_id?: Array<any>;
}
