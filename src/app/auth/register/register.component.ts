import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthenticationServiceService } from 'src/app/services/authentication-service.service';
import { User } from '../../core/models/user';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;
  uploadedFiles: Array<File>;
  public imageUrl;
  constructor(private _sanitizer: DomSanitizer, public fb: FormBuilder,
              private authService: AuthenticationServiceService, private http: HttpClient) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      fullName: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      ConfirmationPassword: new FormControl(null, Validators.required),
      phoneNumber: new FormControl(null, Validators.required),
    });
  }

  get RegisterForm(): any {
    return this.registerForm;
  }

  fileChange(element) {
    this.uploadedFiles = element.target.files;
  }

  onSubmit(): void {

    if (this.registerForm.invalid) {
      return;
    }
    const user = new User();
    user.email = this.registerForm.get('email').value;
    user.fullName = this.registerForm.get('fullName').value;
    user.phoneNumber = this.registerForm.get('phoneNumber').value;
    user.password = this.registerForm.get('password').value;
    const formData = new FormData();
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.uploadedFiles.length; i++) {
      formData.append('uploads[]', this.uploadedFiles[i], this.uploadedFiles[i].name);
    }
    formData.append('email', user.email);
    formData.append('fullName', user.fullName);
    formData.append('phoneNumber', user.phoneNumber);
    formData.append('password', user.password);

    // this.http.post('http://localhost:5000/api/upload', formData)
    //   .subscribe((response) => {
    //     let imageBinary = response['image']; //image binary data response from api
    //     let imageBase64String = btoa(imageBinary);
    //     this.imageUrl = this._sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,'
    //       + imageBinary.toString());
    //   })
    this.authService.register(formData)
      .subscribe((response: User) => { console.log(response) });

  }
}
