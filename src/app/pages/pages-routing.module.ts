import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CreateExamComponent } from './exam/create-exam/create-exam.component';
import { ViewExamComponent } from './exam/view-exam/view-exam.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { TestBankComponent } from './test-bank/test-bank.component';
import { GradesComponent } from './grades/grades.component';
import { StudentGradeViewComponent } from './grades/student-grade-view/student-grade-view.component';
import { DataComponent } from './data/data.component';
import { CreateTestComponent } from './test-bank/create-test/create-test.component';
import { UploadStudentsComponent } from './create-student/upload-students/upload-students.component';
import { CreateStudentComponent } from './create-student/create-student.component';

const routes: Routes = [{
  path: 'dashboard',
  component: DashboardComponent,
  children: [
    {
      path: 'create-exam',
      component: CreateExamComponent
    },
    {
      path: 'view-exam',
      component: ViewExamComponent
    },
    {
      path: 'add-teacher',
      component: AddStudentComponent
    },
    {
      path: 'test-bank',
      component: TestBankComponent,
    },
    {
      path: 'add-course',
      component: GradesComponent
    },
    {
      path: 'students/:id',
      component: StudentGradeViewComponent
    },
    {
      path: 'data',
      component: DataComponent
    },
    {
      path: 'create-test',
      component: CreateTestComponent
    },
    {
      path: 'add-student',
      component: CreateStudentComponent
    }
    ,
    {
      path: 'add-student-upload',
      component: UploadStudentsComponent
    }
  ]
},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
