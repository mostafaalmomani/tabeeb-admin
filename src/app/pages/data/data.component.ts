import { Component, OnInit } from '@angular/core';
import { argv } from 'process';
import { ExamsService } from 'src/app/services/exams.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.scss']
})
export class DataComponent implements OnInit {

  theme: string;
  charts;
  constructor(private cardSercices: ExamsService) { }

  ngOnInit(): void {

    this.cardSercices.getCardsInfo()
      .subscribe(arg => {
        console.log(arg)
        this.charts = {
          title: {
            text: 'Data',
            subtext: '',
            x: 'center',
            top: 50,
            textStyle: {
              color: '#ccc',
              fontSize: '30px'
            },
          },
          tooltip: {
            trigger: 'item',
            // formatter: '{a} <br/>{b} : {c} ({d}%)'
          },
          legend: {
            x: 'center',
            y: 'bottom',
            data: ['Exams', 'Students', 'Teachers','Courses']
          },
          calculable: true,
          visualMap: {
            show: false,
            min: 120,
            max: 600,
            inRange: {
              // colorLightness: [0, 1],
            },
          },
          roseType: 'radius',
          label: {
            normal: {
              textStyle: {
                color: 'rgba(255, 255, 255, 0.3)',
              },
            },
          },
          labelLine: {
            normal: {
              lineStyle: {
                color: 'rgba(255, 255, 255, 0.3)',
              },
              smooth: 0.5,
              length: 15,
              length2: 20,
            },
          },
          itemStyle: {
            normal: {
              color: '#c23531',
              shadowBlur: 200,
              shadowColor: 'rgba(0, 0, 0, 0.5)',
            },
          },

          animationType: 'scale',
          animationEasing: 'elasticOut',
          animationDelay: () => Math.random() * 200,
          series: [
            {
              name: 'Number of',
              type: 'pie',
              radius: [60, 130],
              roseType: 'rose',
              data: [
                { value: arg.teachers, name: 'Teachers' },
                { value: arg.students, name: 'Students' },
                { value: arg.exams, name: 'Exams' },
                { value: arg.courses, name: 'Courses' },

              ]
            }
          ]
        };


      });

  }



}
