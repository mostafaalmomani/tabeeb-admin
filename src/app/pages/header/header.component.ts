import { Component, OnInit } from '@angular/core';
import { AuthenticationServiceService } from 'src/app/services/authentication-service.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  username: string;

  constructor(private authService: AuthenticationServiceService) { }

  ngOnInit(): void {
    this.username = sessionStorage.getItem('fullName');
  }

  logout(): void {
    this.authService.logout();
  }
  deleteAll(): any {
    this.authService.deleteAll()
      .subscribe(arg => { console.log(arg)});
  }
}
