import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Student } from 'src/app/core/models';
import { StudentServicesService } from 'src/app/services/student-services.service';
import { MessageDialogComponent } from 'src/app/shared/message-dialog/message-dialog.component';
import { StudentCardAddEditComponent } from '../add-student/student-card-add-edit/student-card-add-edit.component';
import { StudentEditComponent } from './student-edit/student-edit.component';
import { timer, Subscriber, Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-create-student',
  templateUrl: './create-student.component.html',
  styleUrls: ['./create-student.component.scss']
})
export class CreateStudentComponent implements OnInit ,AfterViewInit{

  displayedColumns: string[] = ['studentName', 'studentNumber', 'level', 'email', 'password', 'update', 'delete'];
  public dataSource;
  source = timer(0, 30);
  subscription: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private fb: FormBuilder, public dialog: MatDialog,
    private studentservice: StudentServicesService, private _snackBar: MatSnackBar) {
      this.dataSource = new MatTableDataSource([]);
    }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.loadStudent();
  }

  private loadStudent() {
    this.studentservice.getStudent()
      .subscribe(arg => {
        console.log(arg.students);
        this.dataSource.data = arg.students;
      });
  }

  addStudent(): void {
    const dialogRef = this.dialog.open(StudentEditComponent, {
      width: '500px',
      height: '510px',
      data: { mode: 0 }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        window.location.reload();
      }
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  edit(student: Student): void {
    const dialogRef = this.dialog.open(StudentEditComponent, {
      width: '500px',
      height: '510px',
      data: { mode: 1, studentObject: student }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        window.location.reload();
      }
    });
  }

  delete(_id: string): void {
    const dialogRef = this.dialog.open(MessageDialogComponent, {
      width: '230px',
      height: '210px',
      data: { title:  'رسالة تأكيد!', bodyMessage: 'هل أنت متأكد من الحذف؟' }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.studentservice.deleteStudent(_id)
          .subscribe(arg => {
            console.log(arg);
            this.openSnackBar('تم الحذف', 'suc-snackbar');
            window.location.reload();
          });

      }
    });
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }


}
