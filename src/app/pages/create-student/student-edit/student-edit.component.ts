import { Component, Inject, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Student, User } from 'src/app/core/models';
import { StudentServicesService } from 'src/app/services/student-services.service';
import { CreateStudentComponent } from '../create-student.component';

@Component({
  selector: 'app-student-edit',
  templateUrl: './student-edit.component.html',
  styleUrls: ['./student-edit.component.scss']
})
export class StudentEditComponent implements OnInit {

  registerForm: FormGroup;
  levels = ['1', '2', '3', '4', '5', '6', 'أخرى'];
  courses = [];
  hide = true;
  constructor(private fb: FormBuilder, private _snackBar: MatSnackBar,
              private studentservice: StudentServicesService,
              public dialogRef: MatDialogRef<CreateStudentComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      fullName: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      phoneNumber: new FormControl(null, Validators.required),
      level: new FormControl(null, Validators.required),
    });
    if (this.data.mode === 1) {
      this.registerForm.controls.fullName.setValue(this.data.studentObject.studentName);
      this.registerForm.controls.email.setValue(this.data.studentObject.email);
      this.registerForm.controls.password.setValue(this.data.studentObject.NonHashedPassword);
      this.registerForm.controls.level.setValue(this.data.studentObject.level);
      this.registerForm.controls.phoneNumber.setValue(this.data.studentObject.phone);
    }
    //console.log(this.data);
  }

  onSubmit(): void {
    if (this.registerForm.invalid) {
      return;
    }
    let student = new Student();
    student.email = this.registerForm.get('email').value;
    student.studentName = this.registerForm.get('fullName').value;
    student.phone = this.registerForm.get('phoneNumber').value;
    student.password = this.registerForm.get('password').value;
    student.level = this.registerForm.get('level').value;
    student._id = this.data.studentObject._id
    if (this.data.mode === 1) {
      this.studentservice.editStudents(student)
        .subscribe(arg => {
          if (arg.message.match('saved!')) {
            this.openSnackBar('تم الحفظ', 'suc-snackbar');
          }
        },
          (error) => {
            console.log(error);
            if (error) {
              this.openSnackBar('الاسم او الرقم موجود مسبقا', 'err-snackbar');
            }
          });
      }
    this.dialogRef.close(true);
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }

}
