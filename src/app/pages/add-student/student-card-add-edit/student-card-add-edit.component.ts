import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { StudentServicesService } from 'src/app/services/student-services.service';
import { AddStudentComponent } from '../add-student.component';
import {
  MatSnackBar,
  MatSnackBarHorizontalPosition,
  MatSnackBarVerticalPosition,
} from '@angular/material/snack-bar';
import { Student, User } from 'src/app/core/models';

@Component({
  selector: 'app-student-card-add-edit',
  templateUrl: './student-card-add-edit.component.html',
  styleUrls: ['./student-card-add-edit.component.scss']
})
export class StudentCardAddEditComponent implements OnInit {

  registerForm: FormGroup;
  levels = ['1', '2', '3', '4', '5', '6', 'أخرى'];
  courses = [];
  hide = true;
  constructor(private fb: FormBuilder, private _snackBar: MatSnackBar,
              private studentservice: StudentServicesService,
              public dialogRef: MatDialogRef<AddStudentComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.studentservice.getCourses()
      .subscribe(arg => {
        this.courses = arg.courses;
      });

    this.registerForm = this.fb.group({
      fullName: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      course_id: new FormControl(null, Validators.required),
      phoneNumber: new FormControl(null, Validators.required),
    });
    if (this.data.mode === 1) {
      this.registerForm.controls.fullName.setValue(this.data.studentObject.fullName);
      this.registerForm.controls.email.setValue(this.data.studentObject.email);
      this.registerForm.controls.password.setValue(this.data.studentObject.purePassword);
      this.registerForm.controls.course_id.setValue(this.data.studentObject.course_id);
      this.registerForm.controls.phoneNumber.setValue(this.data.studentObject.phoneNumber);
    }
    //console.log(this.data);
  }

  onSubmit(): void {
    if (this.registerForm.invalid) {
      return;
    }
    let user = new User();
    user.email = this.registerForm.get('email').value;
    user.fullName = this.registerForm.get('fullName').value;
    user.phoneNumber = this.registerForm.get('phoneNumber').value;
    user.password = this.registerForm.get('password').value;
    user.course_id = this.registerForm.get('course_id').value;
    console.log(user.course_id);
    if (this.data.mode === 0) {
      this.studentservice.register(user)
        .subscribe(arg => {
          console.log(arg);
          if (arg.message.match('saved!')) {
            this.openSnackBar('تم الحفظ', 'suc-snackbar');
          }
        },
          (error) => {
            console.log(error);
            if (error) {
              this.openSnackBar('الاسم او الرقم موجود مسبقا', 'err-snackbar');
            }
          });
      }  else if (this.data.mode === 1) {
        user._id = this.data.studentObject._id;
        this.studentservice.editStudent(user)
        .subscribe(arg => {
          console.log(arg);
          if (arg.message.match('saved!')) {
            this.openSnackBar('تم الحفظ', 'suc-snackbar');
          }
        },
          (error) => {
            console.log(error);
            if (error) {
              this.openSnackBar('الاسم او الرقم موجود مسبقا', 'err-snackbar');
            }
          });
      }
    this.dialogRef.close(true);
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }
}
