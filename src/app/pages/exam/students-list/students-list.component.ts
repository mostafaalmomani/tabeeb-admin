import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Student } from 'src/app/core/models';
import { Mark } from 'src/app/core/models/mark';
import { ExamsService } from 'src/app/services/exams.service';
import { ViewExamComponent } from '../view-exam/view-exam.component';

@Component({
  selector: 'app-students-list',
  templateUrl: './students-list.component.html',
  styleUrls: ['./students-list.component.scss']
})
export class StudentsListComponent implements OnInit {

  public students: Student[] = [];
  displayedColumns: string[] = ['studentName', 'studentNumber', 'level', 'email', 'mark'];
  dataSource: MatTableDataSource<unknown>;
  public examMark: any[] = [];
  public examDesc: any[] = [];
  marks: Mark[] = [];
  public examType: any[] = [
    { value: 1, viewValue: 'أول' },
    { value: 2, viewValue: 'ثاني' },
    { value: 3, viewValue: 'نهائي' },
    { value: 4, viewValue: 'أخرى' },
  ];
  examTypValue;
  constructor(private examsService: ExamsService,
              public dialogRef: MatDialogRef<ViewExamComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    this.examTypValue = this.data.examObject.examType;
    console.log(this.examTypValue);
    this.examsService.getStudents(this.data.examObject._id)
      .subscribe(arg => {
        this.students = arg.students;
        this.examsService.getStudentsMark(this.data.examObject._id)
          .subscribe(arg2 => {
            this.marks = arg2.marks;
            let i = 0;

            this.students.forEach(student => {
              if (this.marks[i]) {
                this.examMark[student._id] = this.marks[i].markPoint;
                this.examDesc[student._id] = this.marks[i].markDesc;
                i++;
              }
            });
          });
        this.dataSource = new MatTableDataSource(arg.students);
        console.log(this.dataSource.data);
      });
  }

  saveMark(id: string): void {
    const mark = new Mark();
    mark.examId = this.data.examObject._id;
    mark.studentsId = id;
    mark.markPoint = this.examMark[id];
    mark.markDesc = this.examDesc[id];
    this.examsService.saveExamMark(mark)
      .subscribe(arg => {
        console.log(arg);
        if (arg.message.match('saved')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
        }
      }, (err) => {
        console.log(err);
        this.openSnackBar('حدث خطأ', 'err-snackbar');
      });
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }
}
