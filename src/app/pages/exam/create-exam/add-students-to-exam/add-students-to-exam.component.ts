import { ViewChild } from '@angular/core';
import { Component, Inject, OnInit } from '@angular/core';
import { MatOption } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatListOption, MatSelectionList } from '@angular/material/list';
import { MatSelect } from '@angular/material/select';
import { Student } from 'src/app/core/models';
import { StudentServicesService } from 'src/app/services/student-services.service';
import { CreateExamComponent } from '../create-exam.component';

@Component({
  selector: 'app-add-students-to-exam',
  templateUrl: './add-students-to-exam.component.html',
  styleUrls: ['./add-students-to-exam.component.scss']
})
export class AddStudentsToExamComponent implements OnInit {

  public students: Student[] = [];
  public term: any;
  public levelModel: any;
  public levels = ['1', '2', '3', '4', '5', '6', 'أخرى'];
  public originalStudents: Student[] = [];
  allSelected = false;
  @ViewChild('Students', { static: false }) studentsRef: MatSelectionList;
  constructor(public dialogRef: MatDialogRef<CreateExamComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any, private studentservice: StudentServicesService) { }


  ngOnInit(): void {
    // this.studentservice.getStudent()
    //   .subscribe(arg => {
    //     this.students = arg.students;
    //     this.originalStudents =  this.students;
    //   });
  }
  onNoClick(): void {
    this.dialogRef.close(false);
  }

  onSubmit(): void {
    const students = [];
    const selectedStudents = this.studentsRef.selectedOptions.selected;
    for (let index = 0; index < selectedStudents.length; index++) {
      const element = selectedStudents[index].value;
      students.push(element);
    }

    this.dialogRef.close(students);
  }

  selectAll(): any {
    this.allSelected = !this.allSelected;
    if (this.allSelected) {
      this.studentsRef.selectAll();
    } else {
      this.studentsRef.deselectAll();
    }
  }

  filter(e): void {
    const temStudents = this.students;
    this.students = this.originalStudents.filter(student =>   student.level === e.value );
  }
}
