import { SelectionModel } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { Exam, Rules, Student } from 'src/app/core/models';
import { Course } from 'src/app/core/models/course';
import { AuthenticationServiceService } from 'src/app/services/authentication-service.service';
import { ExamsService } from 'src/app/services/exams.service';
import { AddStudentsToExamComponent } from './add-students-to-exam/add-students-to-exam.component';
import { UploadQuestionsComponent } from './upload-questions/upload-questions.component';

@Component({
  selector: 'app-create-exam',
  templateUrl: './create-exam.component.html',
  styleUrls: ['./create-exam.component.scss']
})
export class CreateExamComponent implements OnInit {


  public lang: any[] = [
    { value: 'en', viewValue: 'انجليزي' },
    { value: 'ar', viewValue: 'عربي' },
  ];
  public numberOfPages: any[] = [
    { value: '1', viewValue: '1' },
    { value: '2', viewValue: '2' },
  ];
  public examType: any[] = [
    { value: '1', viewValue: 'أول' },
    { value: '2', viewValue: 'ثاني' },
    { value: '3', viewValue: 'نهائي' },
    { value: '4', viewValue: 'أخرى' },
  ];
  examInfoFormGroup: FormGroup;
  rulesFormGroup: FormGroup;
  duration: string;
  public selectedStudents: Student = null;
  public years = [];
  public durationVisable = false;
  public color = 'primary';
  displayedColumns: string[] = ['studentName', 'studentNumber', 'level', 'email', 'password', 'delete'];
  public dataSource;
  selection = new SelectionModel<Element>(true, []);
  tableIsVisable = false;
  public cousreNumbers: Set<any> = new Set();
  public cousreName: Set<any> = new Set();
  public courses: Course[];
  public currentExam: Exam;
  public currentRules: Rules;
  public endDate: Date;
  public startDate: Date;
  MODE = 'regular_exam';
  constructor(private _formBuilder: FormBuilder,
              public dialog: MatDialog,
              private authServices: AuthenticationServiceService,
              private examsServices: ExamsService, private _snackBar: MatSnackBar) {

  }

  ngOnInit(): void {
    this.examInfoFormGroup = this._formBuilder.group({
      examType: new FormControl('', [Validators.required]),
      courseName: new FormControl('', [Validators.required]),
      questionsCount: new FormControl('', [Validators.required]),
      semester: new FormControl('', [Validators.required]),
      courseNumber: new FormControl('', [Validators.required]),
      year: new FormControl('', [Validators.required]),
      fullMark: new FormControl('', [Validators.required]),
      examDate: new FormControl('', [Validators.required]),
      start: new FormControl('', [Validators.required]),
      end: new FormControl('', [Validators.required]),
      duration: new FormControl('', [Validators.required]),
      maxGreade: new FormControl('', [Validators.required]),
      markForEachQ: new FormControl('',[Validators.required])
    });
    this.rulesFormGroup = this._formBuilder.group({
      returnBack: new FormControl(false, [Validators.required]),
      isRandomSequence: new FormControl(false, [Validators.required]),
      numberOfPage: new FormControl('', [Validators.required]),
      examLanguage: new FormControl('', [Validators.required]),
      resultVisable: new FormControl(false, [Validators.required]),
      incorrectAnswersIsVisable: new FormControl(false, [Validators.required]),
      isRequiredQusetions: new FormControl(false, [Validators.required]),

    });
    this.examInfoFormGroup.get('duration').disable();
    this.examInfoFormGroup.get('end').valueChanges.subscribe(ch => {
      const date = new Date().getHours().toLocaleString().toString() + ':' + new Date().getMinutes().toLocaleString().toString();
      const start = this.examInfoFormGroup.get('start').value;
      const end = this.examInfoFormGroup.get('end').value;
      const Dffhour = (Number.parseInt(start.substr(0, start.lastIndexOf(':'))) - Number.parseInt(end.substr(0, end.lastIndexOf(':'))));
      const Diffmin = (Number.parseInt(start.substr(start.lastIndexOf(':') + 1)) - Number.parseInt(end.substr(end.lastIndexOf(':') + 1)));

      const dateExam = this.examInfoFormGroup.get('examDate').value;
      // *********************** end date *****************************
      this.startDate = new Date(dateExam);
      this.startDate.setHours(Number.parseInt(start.substr(0, start.lastIndexOf(':'))));
      this.startDate.setMinutes(Number.parseInt(start.substr(start.lastIndexOf(':') + 1)));
      // *********************** end date *****************************
      this.endDate = new Date(dateExam);
      this.endDate.setHours(Number.parseInt(end.substr(0, end.lastIndexOf(':'))));
      this.endDate.setMinutes( Number.parseInt(end.substr(end.lastIndexOf(':') + 1)));
      var hour = (this.endDate.getHours() - this.startDate.getHours()) <0 ? (this.endDate.getHours() - this.startDate.getHours())*-1: (this.endDate.getHours() - this.startDate.getHours());
      var min = this.startDate.getMinutes() - this.endDate.getMinutes();


      console.log(hour + ":" + min);
      var checkPosistiveHour = 0;
      var checkPosistiveMin = 0;
      if(hour === 1 && min > 0) {
         checkPosistiveHour = 0;
         checkPosistiveMin = min < 0 ? 60 - (min * -1) -60 : 60 - min ;
      } else if (hour > 1 && min >0) {
         checkPosistiveHour = hour < 0 ? (hour * -1) : hour;
         checkPosistiveMin = min < 0 ? (min * -1) -60 : 60 - min;
      } else {
         checkPosistiveHour = hour < 0 ? (hour * -1) : hour;
         checkPosistiveMin = min < 0 ? (min * -1) :  min;
      }

      this.duration = (checkPosistiveHour + ':' + checkPosistiveMin);
      console.log(">>>>>>>>>>>>>>>    ", this.duration);
      this.examInfoFormGroup.controls.duration.setValue(this.duration);
      this.durationVisable = true;
    });
    this.examsServices.getCourses()
      .subscribe(arg => {
        console.log(arg);
        this.courses = arg.courses;
        this.courses.forEach(course => {
          this.cousreNumbers.add(course.courseNumber);
          this.cousreName.add(course.courseName);
        });
      });
    this.yearsRange();
  }

  save(): void {
    let rule = new Rules();
    let exam = new Exam();
    let students = new Student();
    this.examInfoFormGroup.controls.duration.setValue(this.duration);
    rule = this.rulesFormGroup.value;
    exam = this.examInfoFormGroup.value;
    exam.duration = this.duration;
    exam.start = this.startDate;
    exam.end = this.endDate;
    students = this.selectedStudents;
    this.examsServices.createExam(exam, rule, students)
      .subscribe(arg => {
        console.log(arg);
        this.currentExam = arg.exam;
        this.currentRules = arg.rules;
        if (arg.message.match('saved')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
        }
      }, (err) => {
        console.log(err);
        this.openSnackBar('حدث خطأ', 'err-snackbar');
      });
  }

  yearsRange(): any {
    let i = 0;
    for (let index = new Date().getFullYear(); index > 2000 ; index--) {
      this.years[i++] = index;
    }
  }

  uploadFromFile(): any {
    const dialogRef = this.dialog.open(UploadQuestionsComponent, {
      width: '1000px',
      height: '700px',
      data: {MODE: this.MODE, currentExam:  this.currentExam, currentRules: this.currentRules }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  addStudents(): void {
    const dialogRef = this.dialog.open(AddStudentsToExamComponent, {
      width: '400px',
      height: '500px',
      data: { selectedStudents: this.selectedStudents }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.selectedStudents = new Student();
        this.selectedStudents = result;
        this.dataSource = new MatTableDataSource<Element>(result);
        this.tableIsVisable = true;
      }
    });
  }

  deleteStudent(student: Student): void {
    this.dataSource.data = this.dataSource.data
      .filter(i => i !== student)
      .map((i, idx) => (i.position = (idx + 1), i));
    this.dataSource._updateChangeSubscription();
    this.selectedStudents = this.dataSource.data;
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }
}
