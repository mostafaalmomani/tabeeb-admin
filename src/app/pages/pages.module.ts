import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule } from './pages-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { CreateExamComponent } from './exam/create-exam/create-exam.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { ViewExamComponent } from './exam/view-exam/view-exam.component';
import { UploadQuestionsComponent } from './exam/create-exam/upload-questions/upload-questions.component';
import { TestBankComponent } from './test-bank/test-bank.component';
import { HeaderComponent } from './header/header.component';
import { InterceptorService } from '../loader/interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CreateTestComponent } from './test-bank/create-test/create-test.component';
import { StudentCardAddEditComponent } from './add-student/student-card-add-edit/student-card-add-edit.component';
import { AddStudentsToExamComponent } from './exam/create-exam/add-students-to-exam/add-students-to-exam.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { GradesComponent } from './grades/grades.component';
import { StudentsListComponent } from './exam/students-list/students-list.component';
import { AddEditCourseComponent } from './grades/add-edit-course/add-edit-course.component';
import { StudentGradesComponent } from './grades/student-grades/student-grades.component';
import { StudentGradeViewComponent } from './grades/student-grade-view/student-grade-view.component';
import { NgxEchartsModule } from 'ngx-echarts';
import * as echarts from 'echarts';
import { DataComponent } from './data/data.component';
import { QuillModule } from 'ngx-quill';
import { CreateStudentComponent } from './create-student/create-student.component';
import { UploadStudentsComponent } from './create-student/upload-students/upload-students.component';
import { StudentEditComponent } from './create-student/student-edit/student-edit.component';


@NgModule({
  declarations: [
    DashboardComponent,
    CreateExamComponent,
    AddStudentComponent,
    ViewExamComponent,
    UploadQuestionsComponent,
    HeaderComponent,
    TestBankComponent,
    CreateTestComponent,
    StudentCardAddEditComponent,
    AddStudentsToExamComponent,
    GradesComponent,
    StudentsListComponent,
    AddEditCourseComponent,
    StudentGradesComponent,
    StudentGradeViewComponent,
    DataComponent,
    CreateStudentComponent,
    UploadStudentsComponent,
    StudentEditComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    SharedModule,
    NgxEchartsModule.forRoot({
      echarts,
   }),
    Ng2SearchPipeModule,
    QuillModule
  ],
  exports: [],
  providers: [
    // { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true }
  ],
  entryComponents: [UploadQuestionsComponent, AddStudentsToExamComponent]
})
export class PagesModule { }
