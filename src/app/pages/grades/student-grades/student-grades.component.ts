import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Mark } from 'src/app/core/models/mark';
import { MarkCourse } from 'src/app/core/models/markCourse';
import { ExamsService } from 'src/app/services/exams.service';
import { StudentGradeViewComponent } from '../student-grade-view/student-grade-view.component';

@Component({
  selector: 'app-student-grades',
  templateUrl: './student-grades.component.html',
  styleUrls: ['./student-grades.component.scss']
})
export class StudentGradesComponent implements OnInit {

  marks: Mark[] = [];
  marksModel: any[] = [];
  sum: number = 0;
  originalSum = 0;
  otherMark: any;
  studentId: any;
  courseNumber: string;
  point: string;
  courseId: string;
  constructor(public dialogRef: MatDialogRef<StudentGradeViewComponent>, private examsService: ExamsService,
    @Inject(MAT_DIALOG_DATA) public data: any, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {
    console.log(this.data);
    this.initalze();

  }

  saveOtherMark(): any {
    const mark = new Mark();
    mark.courseNumber = this.courseNumber;
    mark.markDesc = 'others';
    mark.markPoint = this.otherMark;
    mark.studentsId = this.studentId;
    console.log(mark);
    this.examsService.saveOtherMark(mark)
      .subscribe(arg => {
        console.log(arg);
        if (arg.message.match('saved')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
          this.update();
          this.point = this.findRangePoint(this.data.course.scales);
          this.saveCourseMark();
        }
      }, (err) => {
        console.log(err);
        this.openSnackBar('حدث خطأ', 'err-snackbar');
      });
  }

  update(): void {
    this.examsService.getStudentMarks(this.studentId, this.courseNumber)
      .subscribe(arg => {
        this.sum = 0;
        arg.marks.forEach(element => {
          this.sum += Number.parseFloat(element.markPoint);
          this.point = this.findRangePoint(this.data.course.scales);
          this.originalSum = this.sum;
        });
      });
  }

  updateMark(markId: string): any {
    let marks = this.marks.filter(element => element._id === markId);
    const mark = new Mark();
    mark._id = marks[0]['_id'];
    mark.courseNumber = marks[0]['courseNumber'];
    mark.examId = marks[0]['examId'];
    mark.markDesc = marks[0]['markDesc'];
    mark.markPoint = this.marksModel[markId];
    mark.studentsId = marks[0]['studentsId'];
    this.examsService.saveExamMark(mark)
      .subscribe(arg => {
        console.log(arg);
        if (arg.message.match('saved')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
          this.sum += Number.parseFloat(arg.mark.markPoint);
          this.point = this.findRangePoint(this.data.course.scales);
        }
      }, (err) => {
        console.log(err);
        this.openSnackBar('حدث خطأ', 'err-snackbar');
      });
  }

  saveCourseMark(): any {
    const markCourse = new MarkCourse();
    markCourse.courseNumber = this.courseNumber;
    markCourse.studentsId = this.studentId;
    markCourse.grade = this.sum;
    markCourse.markPoint = this.findRangePoint(this.data.course.scales);
    markCourse.markDesc = '';
    this.examsService.saveCourseMark(markCourse)
      .subscribe(arg => {
        console.log(arg);
        if (arg.message.match('saved')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
        }
      }, (err) => {
        console.log(err);
        this.openSnackBar('حدث خطأ', 'err-snackbar');
      });

  }

  changeExamsMarks(id: string): any {

    if (this.marksModel[id]) {
      console.log('mark exam   ', this.marksModel[id])
      this.sum += Number.parseFloat(this.marksModel[id]);
      console.log("sum  ", this.sum)
    } else {
      this.sum = this.originalSum;
    }
    this.point = this.findRangePoint(this.data.course.scales);
  }

  initalze(): void {
    this.sum = 0;
    this.courseNumber = this.data.courseId;
    this.studentId = this.data.student;
    this.marks = this.data.marks.filter(element => element.markDesc.match('def'));
    const others = this.data.marks.filter(element => element.markDesc.match('others'));
    this.otherMark = others.length > 0 ? others[0].markPoint : 0;
    this.point = this.findRangePoint(this.data.course.scales);
    this.marks.forEach(element => {
      this.marksModel[element._id] = element.markPoint;
    });

    this.examsService.getStudentCourseMarks(this.studentId, this.courseNumber)
      .subscribe(arg => {
        console.log(arg.mark);
        this.data.marks.forEach(element => {
          this.sum += Number.parseFloat(element.markPoint);
          this.originalSum = this.sum;
          this.point = this.findRangePoint(this.data.course.scales);
        });

        // if (arg.mark.markPoint) {
        //   this.point = arg.mark.markPoint;
        // } else {
        //   this.point = this.findRangePoint(this.data.course.scales);

        // }
      });
  }

  onNoClick(): void {
    this.dialogRef.close(false);
  }

  findRangePoint(points: any[]): any {

    let total = this.sum;
    let degree = points.filter(point => (total >= Number.parseFloat(point.min)) && (total <= Number.parseFloat(point.max)));
    console.log('degree  ', degree);
    if (degree.length > 0) {
      return degree[0]['degree'];
    }

    return 'out of the range';
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }
}
