import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute } from '@angular/router';
import { Exam } from 'src/app/core/models';
import { Course } from 'src/app/core/models/course';
import { ExamsService } from 'src/app/services/exams.service';
import { AddEditCourseComponent } from './add-edit-course/add-edit-course.component';
import { timer, Subscriber, Observable } from 'rxjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-grades',
  templateUrl: './grades.component.html',
  styleUrls: ['./grades.component.scss']
})
export class GradesComponent implements OnInit ,AfterViewInit {

  displayedColumns: string[] = [
    'courseName', 'courseNumber',
    'created', 'delete'
  ];
  public dataSource;
  public term: any;
  public cousreNumbers: Set<any> = new Set();
  public cousreName: Set<any> = new Set();
  public courses: Course[];
  source = timer(0, 30);
  subscription: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private route: ActivatedRoute, private examsService: ExamsService,
              public dialog: MatDialog, private _snackBar: MatSnackBar) {
                this.dataSource = new MatTableDataSource([]);
              }

  ngAfterViewInit(): void {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.loadCourses();
  }

  private loadCourses() {
    this.examsService.getCourses()
      .subscribe(arg => {
        console.log(arg);
        this.courses = arg.courses;
        this.dataSource.data = arg.courses;
        this.courses.forEach(course => {
          this.cousreNumbers.add(course.courseNumber);
          this.cousreName.add(course.courseName);
        });
      });
  }

  courseNumberFilter(e): void {
    console.log(e.value);
    const temStudents = this.courses;
    this.dataSource.data = this.courses.filter(exam => exam.courseNumber === e.value);
  }

  kindOfExamFilter(e): void {
    console.log(e.value);
    if(e.value === 0) {
      this.dataSource = this.courses;
      return;
    }
    const temStudents = this.courses;
    this.dataSource = this.courses.filter(course => course.courseName === e.value);
  }

  addCourse(): any {
    const dialogRef = this.dialog.open(AddEditCourseComponent, {
      width: '500px',
      height: '560px',
      data: { mode: 0 }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      if (result) {
        this.subscription = this.source.subscribe(val => {
          this.loadCourses();
          this.subscription.unsubscribe();
        });
      }
    });
  }

  delete(courseid: string): any {
    this.examsService.deleteCourse(courseid)
      .subscribe(arg => {
        if(arg.message.match('done')) {
          this.openSnackBar('تم الحفظ', 'suc-snackbar');
          this.subscription = this.source.subscribe(val => {
            this.loadCourses();
            this.subscription.unsubscribe();
          });
        }
      }, (err) => {
        console.log(err);
        this.openSnackBar('حدث خطأ', 'err-snackbar');
      });
  }


  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }
}
