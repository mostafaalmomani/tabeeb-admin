import { ThrowStmt } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatTableDataSource } from '@angular/material/table';
import { ActivatedRoute } from '@angular/router';
import { Student } from 'src/app/core/models';
import { Course } from 'src/app/core/models/course';
import { Mark } from 'src/app/core/models/mark';
import { MarkCourse } from 'src/app/core/models/markCourse';
import { ExamsService } from 'src/app/services/exams.service';
import { StudentGradesComponent } from '../student-grades/student-grades.component';

@Component({
  selector: 'app-student-grade-view',
  templateUrl: './student-grade-view.component.html',
  styleUrls: ['./student-grade-view.component.scss']
})
export class StudentGradeViewComponent implements OnInit {

  courseId: any;
  public dataSource;
  public courseName = '';
  public students: Student[] = [];
  public originalStudents: Student[] = [];
  public studentMark: any[] = [];
  public studentOtherMark: any[] = [];
  public points: any[] = [];
  public markEntity: MarkCourse[] = [];
  public otherMarkEntity: Mark[] = [];
  public term: any;
  course: Course;
  public levels = ['1', '2', '3', '4', '5', '6', 'أخرى'];
  displayedColumns: string[] =
  ['studentName', 'studentNumber', 'level', 'email', 'degree', 'total'];
  property: any;
  sum: any;

  constructor(private route: ActivatedRoute, private examsService: ExamsService,
              public dialog: MatDialog, private _snackBar: MatSnackBar) { }

  ngOnInit(): void {

    this.route.params.subscribe(params => {
      this.courseId = params.id;
      this.loadAssessments();
      this.loadMarks();
    });
  }

  private loadAssessments() {
    debugger
    this.examsService.getStudentAssessments(this.courseId)
      .subscribe(arg => {
        console.log(arg.assessments);
        this.dataSource = new MatTableDataSource(arg.assessments);
        arg.assessments.forEach(element => {
          this.studentMark[element.studentsId._id] = element.finalSum;
          this.studentOtherMark[element.studentsId._id] = element.otherExamMark;
          this.points[element.studentsId._id] = this.findRangePoint(element.course.scales,element.finalSum);
        });

      });
  }

  findRangePoint(points: any[], sum: any): any {
    let total = sum;
    let degree = points.filter(point => (total >= Number.parseFloat(point.min)) && (total <= Number.parseFloat(point.max)));
    console.log('degree  ', degree);
    if (degree.length > 0) {
      return degree[0]['degree'];
    }
    return 'هـ';
  }

  private loadMarks() {
    this.examsService.getCoursesStudents(this.courseId)
      .subscribe(arg => {
        this.courseName = arg.courseName;
        this.students = arg.students;
        this.originalStudents = arg.students;
        this.course = arg.course;
        this.loadStudentsMarks();
        // this.dataSource = new MatTableDataSource(arg.students);
        this.students.forEach(s => {
          // this.studentMark[s._id] = 0;
          // this.studentOtherMark[s._id] = 0;
        });
      });
  }

  private loadStudentsMarks() {
    this.examsService.getStudentMarks('123', this.course.courseNumber)
      .subscribe(arg => {
        console.log(arg);
        this.property = arg.marks
      });
  }

  openMark(studentId: string): void {
    this.examsService.getStudentMarks(studentId, this.courseId)
      .subscribe(arg => {
        const dialogRef = this.dialog.open(StudentGradesComponent, {
          width: '380px',
          height: '390px',
          data: { marks: arg.marks, courseId: this.courseId, student: studentId, course: this.course ,courseName: this.courseName}
        });

        dialogRef.afterClosed().subscribe(result => {
          console.log('The dialog was closed');
          if (result) {
            // window.location.reload();
          }
        });
      });
  }


  applyFilter(event: Event): void {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  clear(): any {
    this.dataSource = this.originalStudents;
  }
  filter(e): void {
    // this.students = this.originalStudents;
    const temStudents = this.students;
    this.dataSource = this.originalStudents.filter(student => student.level === e.value);
  }

  openSnackBar(message: string, syle: string): void {
    this._snackBar.open(message, '', {
      duration: 500,
      panelClass: [syle]
    });
  }
}
