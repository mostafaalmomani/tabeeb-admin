import { Component, ElementRef, Input, OnInit } from '@angular/core';
import jsPDF from 'jspdf';
import 'jspdf-autotable';

@Component({
  selector: 'app-export-pdf',
  templateUrl: './export-pdf.component.html',
  styleUrls: ['./export-pdf.component.scss']
})
export class ExportPdfComponent implements OnInit {
  // @ViewChild('htmlData') htmlData:ElementRef;
  @Input() head;
  @Input() data;
  @Input() title;

  constructor() { }

  ngOnInit(): void {
  }

  createPdf() {
    let doc = new jsPDF('p','pt', 'a4');

    doc.setFontSize(18);
    doc.text('My Team Detail', 11, 8);
    doc.setFontSize(11);
    doc.setFont('normal');
    doc.setLanguage("ar-JO");
    doc.setTextColor(100);
    (doc as any).autoTable({
      head: this.head,
      body: this.data,
    })
    doc.save('myteamdetail.pdf');
  }
}


