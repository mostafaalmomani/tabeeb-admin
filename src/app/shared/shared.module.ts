import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from './material/material.module';
import { MessageDialogComponent } from './message-dialog/message-dialog.component';
import { QuillModule } from 'ngx-quill';
import { ExportPdfComponent } from './export-pdf/export-pdf.component';



@NgModule({
  declarations: [MessageDialogComponent, ExportPdfComponent],
  imports: [
    CommonModule,
    MaterialModule,
    QuillModule
  ],
  exports:[
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ExportPdfComponent
  ]
})
export class SharedModule { }
